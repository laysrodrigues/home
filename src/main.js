import Vue from 'vue'
import './plugins/vuetify'
import VueAnalytics from 'vue-analytics'
import App from './App.vue'

import router from './router/routes'

Vue.config.productionTip = false
const isProd = process.env.NODE_ENV === 'production'

Vue.use(VueAnalytics, {
  id: 'UA-103612095-5',
  router,
  autoTracking: {
   pageviewOnLoad: false
 },
 debug: {
   enabled: !isProd,
   sendHitTask: isProd
 },
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
