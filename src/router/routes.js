import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import ('@/pages/home.vue')
  },
  {
    path: '/tips',
    name: 'tips',
    component: () => import ('@/pages/tips.vue')
  },
]

export default new VueRouter({routes})
